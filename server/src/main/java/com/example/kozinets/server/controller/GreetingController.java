package com.example.kozinets.server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/hello")
public class GreetingController {

    @GetMapping
    public String hello() {
        return "hello";
    }

}
