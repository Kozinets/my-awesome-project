package com.example.kozinets.server.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.kozinets.server.model.Employee;
import com.example.kozinets.server.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmployeeQueries implements GraphQLQueryResolver {

    private final EmployeeRepository repository;

    @Autowired
    public EmployeeQueries(EmployeeRepository repository) {
        this.repository = repository;
    }

    public List<Employee> employees() {
        return repository.findAll();
    }

    public List<Employee> employeesByName(String name) {
        return repository.findByName(name);
    }

    public List<Employee> employeesByDescription(String description) {
        return repository.findByDescription(description);
    }

    public Employee employee(Long id) {
        return repository.findById(id);
    }
}
