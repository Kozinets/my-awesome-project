package com.example.kozinets.server.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example.kozinets.server.model.Employee;
import com.example.kozinets.server.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeMutations implements GraphQLMutationResolver {

    private final EmployeeRepository repository;

    @Autowired
    public EmployeeMutations(EmployeeRepository repository) {
        this.repository = repository;
    }

    public Employee newEmployee(String name, String description) {
        return repository.add(new Employee(name, description));
    }

    public boolean deleteEmployee(Long id) {
        return repository.delete(id);
    }

    public Employee updateEmployee(Long id, Employee employee) {
        return repository.update(id, employee);
    }

}
