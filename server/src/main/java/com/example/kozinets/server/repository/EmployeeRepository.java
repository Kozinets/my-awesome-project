package com.example.kozinets.server.repository;

import com.example.kozinets.server.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private static List<Employee> employeeList = new ArrayList<>();

    public Employee findById(Long id) {
        Optional<Employee> employee = employeeList.stream().filter(a -> a.getId().equals(id)).findFirst();
        return employee.orElse(null);
    }

    public List<Employee> findAll() {
        return employeeList;
    }

    public List<Employee> findByName(String name) {
        return employeeList.stream().filter(a -> a.getName().equals(name)).collect(Collectors.toList());
    }

    public List<Employee> findByDescription(String description) {
        return employeeList.stream().filter(a -> a.getDescription().equals(description)).collect(Collectors.toList());
    }

    public Employee add(Employee employee) {
        employee.setId((long) (employeeList.size() + 1));
        employeeList.add(employee);
        return employee;
    }

    public boolean delete(Long id) {
        return employeeList.removeIf(it -> it.getId().equals(id));
    }

    public Employee update(Long id, Employee employee) {
        employee.setId(id);
        int index = employeeList.indexOf(employee);
        employeeList.set(index, employee);
        return employee;
    }
}
