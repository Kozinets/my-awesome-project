package com.example.kozinets.client.graphql;

import java.util.Map;

public final class Request {

    private String query;
    private Map<String, Object> variables;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }

    @Override
    public String toString() {
        return "Request{" +
                "query='" + query + '\'' +
                ", variables=" + variables +
                '}';
    }
}