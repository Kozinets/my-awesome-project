package com.example.kozinets.client.controller;

import com.example.kozinets.client.client.AwesomeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    private final AwesomeClient awesomeClient;

    @Autowired
    public EmployeeController(AwesomeClient awesomeClient) {
        this.awesomeClient = awesomeClient;
    }

    @GetMapping("/hello")
    public String greeting() {
        return awesomeClient.greeting();
    }
}
