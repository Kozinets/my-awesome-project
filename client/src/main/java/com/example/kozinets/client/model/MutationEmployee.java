package com.example.kozinets.client.model;

import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(name = "newEmployee", arguments = {@GraphQLArgument(name = "name"),
        @GraphQLArgument(name = "description")})
public class MutationEmployee {

    private String name;

    private String description;

    public MutationEmployee() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}