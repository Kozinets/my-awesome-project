package com.example.kozinets.client.controller;

import com.example.kozinets.client.graphql.GraphQLFeignTemplate;
import com.example.kozinets.client.model.Employee;
import com.example.kozinets.client.model.MutationEmployee;
import io.aexp.nodes.graphql.Argument;
import io.aexp.nodes.graphql.Arguments;
import io.aexp.nodes.graphql.GraphQLRequestEntity;
import io.aexp.nodes.graphql.GraphQLTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;

@RestController("/employees")
public class GraphQLController {

    @Value("${awesome.server.url}")
    private String serverUrl;

    private final GraphQLFeignTemplate feignTemplate;

    @Autowired
    public GraphQLController(GraphQLFeignTemplate feignTemplate) {
        this.feignTemplate = feignTemplate;
    }

    @GetMapping
    public String employees() throws MalformedURLException {
        GraphQLRequestEntity requestEntity = GraphQLRequestEntity.Builder()
                .url(serverUrl)
                .request(Employee.class)
                .build();

        return feignTemplate.execute(requestEntity);
    }

    @PostMapping
    public String createEmployee(@RequestBody MutationEmployee employee) throws MalformedURLException {
        GraphQLRequestEntity requestEntity = GraphQLRequestEntity.Builder()
                .url(serverUrl)
                .arguments(new Arguments("newEmployee",
                        new Argument<>("name", employee.getName()),
                        new Argument<>("description", employee.getDescription())))
                .requestMethod(GraphQLTemplate.GraphQLMethod.MUTATE)
                .request(MutationEmployee.class)
                .build();

        return feignTemplate.execute(requestEntity);
    }
}
