package com.example.kozinets.client.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(url = "${awesome.server.url}", name = "${awesome.server.name}")
public interface AwesomeClient {

    @GetMapping("/hello")
    String greeting();

    @PostMapping("/graphql")
    feign.Response graphql(String request);
}
