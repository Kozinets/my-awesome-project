package com.example.kozinets.client.graphql;

import io.aexp.nodes.graphql.GraphQLRequestEntity;
import io.aexp.nodes.graphql.GraphQLTemplate;
import io.aexp.nodes.graphql.exceptions.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GraphQLFeignTemplate {

    private final Fetch fetch;

    @Autowired
    public GraphQLFeignTemplate(Fetch fetch) {
        this.fetch = fetch;
    }

    public String execute(GraphQLRequestEntity requestEntity) throws GraphQLException {
        return execute(null, requestEntity);
    }

    public String execute(GraphQLTemplate.GraphQLMethod method, GraphQLRequestEntity requestEntity) {
        if (null == requestEntity) {
            throw new GraphQLException("requestEntity must not be null");
        } else {
            if (null != method) {
                requestEntity.setRequestMethod(method);
            }

            return fetch.send(requestEntity);
        }
    }
}
