package com.example.kozinets.client.graphql;

import com.example.kozinets.client.client.AwesomeClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.aexp.nodes.graphql.GraphQLRequestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Fetch {

    private final AwesomeClient awesomeClient;

    private final ObjectMapper mapper;

    @Autowired
    public Fetch(AwesomeClient awesomeClient, ObjectMapper mapper) {
        this.awesomeClient = awesomeClient;
        this.mapper = mapper;
    }


    public String send(GraphQLRequestEntity requestEntity) {
        Request request = new Request();
        request.setQuery(requestEntity.getRequest());
        request.setVariables(requestEntity.getVariables());

        String requestParams = convertToString(request);
        return awesomeClient.graphql(requestParams).toString();
    }

    private String convertToString(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
